'use strict';

const path = require('path');
const helmet = require('helmet');
const conf = require('./config/conf');

/**
 * Подключаем необходимый шаблонизатор и папку со статикой
 * @constructor
 */
conf.app.set('views', path.join(__dirname, 'views'));
conf.app.set('view engine', 'pug');
conf.app.use(conf.express.static(path.join(__dirname, 'public')));
conf.app.use(helmet());

// require('./api/conf.js');
module.exports = conf.app;
