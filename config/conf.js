'use strict';
/**
 * Конфигурация
 * @param {object} path 
 * @param {object} express 
 * @param {object} logger
 * @param {object} cookieParser
 * @param {object} compression
 * @param {object} helmet
 * @param {object} router
 * @param {object} jwt
 * @param {object} bcrypt
 * @param {object} WebSocketServer
 * @param {object} wss
 * @param {object} scriptSources
 * @param {object} styleSources
 * @param {object} connectSources
 */
const path = require('path');
//Сам экспресс
const express = require('express');
// логер
const logger = require('morgan');
// Парсинг куков
const cookieParser = require('cookie-parser');
// Сжатие всех данных для более быстрой работы
const compression = require('compression');
const helmet = require('helmet');
// Основа
const router = new express.Router();
// JWT
const jwt = require('jsonwebtoken');
// для криптографии
const bcrypt = require('bcryptjs');
// Websockets 
const WebSocketServer = new require('ws');
const wss = new WebSocketServer.Server({
    port: 8081,
    autoAcceptConnections: false,
    perMessageDeflate: false
});


const app = express();

app.set('jwtTokenSecret', 'proplast.ru');

const scriptSources = ["'self'", "'unsafe-inline'", "'unsafe-eval'", "ajax.googleapis.com"];
const styleSources = ["'self'", "'unsafe-inline'", "ajax.googleapis.com"];
const connectSources = ["'self'", "ws://localhost:8081"];

app.use(helmet());
app.use(helmet.noCache());
app.use(helmet.frameguard());
app.use(helmet.contentSecurityPolicy({
    directives: {
        defaultSrc: ["'self'"],
        // styleSrc: ["'self'", 'maxcdn.bootstrapcdn.com'],
        connectSrc: connectSources,
        // reportUri: '/report-violation',
        // objectSrc: [], // An empty array allows nothing through
    }
}))

app.use(compression());
app.use(cookieParser());
// //Парсер страниц в читаемый вид
const bodyParser = require('body-parser');
// // Логи
router.use(logger('dev'));
// // Библиотечка для парсинга Json
router.use(bodyParser.json());
// // библиотечка для парсинга форм
router.use(bodyParser.urlencoded({ extended: false })); // Parse forms
// // Парсинг куков
router.use(cookieParser());
// // Тестовый middleware
module.exports.app = app;
module.exports.express = express;
module.exports.router = router;
module.exports.wss = wss;
module.exports.jwt = jwt;
module.exports.bcrypt = bcrypt;
module.exports.secret = {
    'secret': 'M@sdklGk09hJ'
};