'use strict';
/**
 * Конфигурация
 * @constructor
 * @param {Object} [mongoose="mongoose objects"] 
 * @param {Object} [databases="dev.json"]
 */
const mongoose = require('mongoose');
let databases = undefined;
if (process.env.NODE_ENV == "test")
    databases = require("./conf_db/test.json");
else {
    databases = require("./conf_db/dev.json");
}
const Schema = mongoose.Schema
const mongoDB = databases.DBHost;

mongoose.connect(mongoDB, {
});

mongoose.Promise = global.Promise;

const db = mongoose.connection;

db.on('error', function (err) {
    console.error('connection error:', err.message);
});

db.once('open', function callback () {
    console.info("Connected to DB!");
});

module.exports.Schema = Schema;
module.exports.mongoose = mongoose;