'use strict';
/**
 * Middleware авторизации
 * @constructor
 * @param {object} token 
 * @param {object} jwt
 */
const {jwt,secret} =  require('../conf.js');
// Проверка на доступ
const verifySecurity = function(req,res,next){
    var token = req.cookies;
    if (token['token']){
        jwt.verify(token['token'], secret.secret, function(err, decoded) {
            if (!decoded) {
                res.cookie('token', '');
                res.redirect('/login');
            } else {
                const User = require("../../models/accounts.js").AccountsModel;
                User.findById(decoded.id, function (err, user) {
                    if (err || !user){
                        res.cookie('token', '');
                        next();
                    } else {
                        res.redirect('/');
                    }
                })
            }
        })
    } else {
        next();
    }
}
module.exports.verifySecurity = verifySecurity;