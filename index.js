'use strict';
const path = require('path');
const helmet = require('helmet');
const conf = require('./config/conf');
// Всякий файловый мусор
conf.app.set('views', path.join(__dirname, 'views'));
conf.app.set('view engine', 'pug');
conf.app.use(conf.express.static(path.join(__dirname, 'public')));
conf.app.use(helmet());

require('./api/conf.js');
require('./router/index.js');
module.exports = conf.app;
