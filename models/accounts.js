'use strict';
const conf = require('../config/database.js');
const Schema = conf.Schema;
const mongoose = conf.mongoose;
/**
 * Модель с департаментами
 * @constructor
 * @param {String} [names="Отдел программистов"] ссылка на соответствующий документ(договор)
 * @param {Object} [accounts="refs"] Люди которые входят в этот отдел
 */
const Department = new Schema(
    {
        names: String,
        accounts: [{
            type: mongoose.Schema.Types.ObjectId, 
            ref: 'Accounts',
        }]
    }
)
const DepartmentModel = mongoose.model('Department', Department);

/**
 * Модель с Должностями
 * @constructor
 * @param {String} [names="Программист"] Название должности
 * @param {String} [office="23"] какой офис
 */
const Position = new Schema(
    {
        names: {
            type: String,
            unique: true
        },
        office: String,
    }
)
const PositionModel = mongoose.model('Position', Position);

/**
 * Модель с пользователями(работниками)
 * @constructor
 * @param {String} [fio="ФИО"] ФИО
 * @param {Number} [age=23] возраст
 * @param {String} [login="hack23"] Логин
 * @param {String} [email="admin@mail.ru"] эл. почта
 * @param {String} [password="sdadsad"] пароль
 * @param {Number} [phone=89992223311] номер телефона
 * @param {Object} [ava="File"] аватарка
 * @param {Object} [position="ref"] должность работника
 * @param {Object} [information={}] полная информация работника 
 */
const Accounts = new Schema(
    {
        fio: {
            type: {type: String},
        },
        age: {type: Number},
        login: {type: String, index: true, unique: true, required: true},
        email: {
            type: String, 
            index: true, 
            unique: true, 
            required: true,
        },
        password: {
            type: String, 
            index: true, 
            unique: true, 
            required: true,
            minlength: 6
        },
        phone: {
            type: Number, 
            index: true, 
            unique: true, 
            required: true,
        },
        ava: {
            type: {
                uploaded: {
                    type: Date,
                    default: Date.now()
                },
                src: String
            }
        },
        position: {
            type: mongoose.Schema.Types.ObjectId, 
            ref: 'Position'
        },
        // Информация о сотруднике
        informatiom: {
            address: {
                city: String,
                // улица
                street: String,
                house: Number,
            },
            passport: {
                seria: Number,
                nomer: Number
            },
            // Прошлые места работы
            past_jobs: {
                company: {
                    name: String,
                    phone: Number,
                },
                position: {
                    type: mongoose.Schema.Types.ObjectId, 
                    ref: 'Position',
                }
            },
            // Судимость
            conviction: {
                body: String,
                type: Boolean,
            },
        },
        //

        date_create: {
            type: Date,
            default: Date.now
        },

        date_update: {
            type: Date,
            default: Date.now
        }

    }
)

const AccountsModel = mongoose.model('Accounts', Accounts);

module.exports.AccountsModel = AccountsModel;
module.exports.PositionModel = PositionModel;
module.exports.DepartmentModel = DepartmentModel;