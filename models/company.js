'use strict';
const conf = require('../config/database.js');
const Schema = conf.Schema;
const mongoose = conf.mongoose;

let address = {
    city: String,
    region: String,
    street: String,
    house: Number,
};
/**
 * Модель с Компаниями
 * @constructor
 * @param {String} [names="ООО Рога и Копыта"] {string} название компании.
 * @param {Object} [information={}] полная информация компании(инн огрн и т.п.). 
 * @param {Object} [branch={}] филиалы
 * @param {Object} [director="refs"] директор компании
 * @param {Object} [transactions="ref"] Транзакции компании
 * @param {Object} [documents="ref"] Договора компании
 * @param {Object} [careers={} ] вакансии компании
 */
const Company = new Schema (
    {
        names: String,
        information: {
            inn: {
                type: Number,
                unique: true,
            },
            kpp: {
                type: Number,
                unique: true,
            },
            ogrn: {
                type: Number,
                unique: true,
            },
            bik: {
                type: Number,
                unique: true,
            },
            // корреспондентский счет
            ks: Number,
            // расчетный счет
            rs: Number,
            address: address,
        },
        // филлиалы
        branch: {
            name: String,
            kpp: Number,
            address: address,
        },
        // Сотрудники компании
        accounts_job: [{
            type: mongoose.Schema.Types.ObjectId, 
            ref: 'Accounts',
        }],
        // директор компании
        director: {
            type: mongoose.Schema.Types.ObjectId, 
            ref: 'Accounts',
        },

        // Транзакции
        transactions: [{
            type: mongoose.Schema.Types.ObjectId, 
            ref: 'Transaction',
        }],
        // договора
        documents: [{
            type: mongoose.Schema.Types.ObjectId, 
            ref: 'Documents',
        }],
        // Вакансии
        careers: [{
            name: String,
            body: String,
            pay: Number,
        }],

        date_create: {
            type: Date,
            default: Date.now
        },

        date_update: {
            type: Date,
            default: Date.now
        },
    }
)


const CompanyModel = mongoose.model('Company', Accounts);

module.exports.CompanyModel = CompanyModel;