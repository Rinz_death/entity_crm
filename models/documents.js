'use strict';
const conf = require('../config/database.js');
const Schema = conf.Schema;
const mongoose = conf.mongoose;

/**
 * Модель с Клиентами
 * @constructor
 * @param {String} [fio="ФИО"]
 * @param {Object} [passport={seria:2323, nomer:2323}]
 * @param {Number} [age=20] 
 * @param {Object} [address={city:"МСК", street:"улица", house:2}]
 */

// Карточка клиентов(физ лица)
const Clients = new Schema({
    fio: String,
    passport: {
        seria: Number,
        nomer: Number
    },
    age: Number,
    address: {
        city: String,
        street: String,
        house: Number,
    },

});
const ClientsModel = mongoose.model('Clients', Clients);

/**
 * Модель с договорами
 * @constructor
 * @param {String} [names="Купли продажи"]
 * @param {Number} [number="2323"]
 * @param {Object} [company_customer="refs"] Заказчик юр лицо
 * @param {Object} [client_fiz="refs"] Заказчик физ лицо
 * @param {Object} [company_executor="refs"] испольнитель
 * @param {String} [types="Продажа"] Тип договора
 * @param {Object} [ref_documents="refs"] Ссылка на связанные договора
 * @param {Date} [terms="22.02.2012"] сроки
 * @param {Number} [summs=23000] Сумма
 */

//Договора
const Documents = new Schema(
    {
        names: String,
        body: {
            title: String,
            text: String,
        },
        // Номер договора
        number: String,
        // Заказчик компания
        company_customer: {
            type: mongoose.Schema.Types.ObjectId, 
            ref: 'Company',
        },
        // Заказчик физ лицо
        client_fiz: {
            type: mongoose.Schema.Types.ObjectId, 
            ref: 'Clients',
        },  
        
        // исполнитель
        company_executor: {
            type: mongoose.Schema.Types.ObjectId, 
            ref: 'Company',
        },

        // тип документа
        types: {
            type: String,
        },
        // Документ может быть связан с другими документами
        ref_documents: [{
            type: mongoose.Schema.Types.ObjectId, 
            ref: 'Documents',
        }],
        // Сроки
        terms: {
            type: Date,
            default: Date.now
        },
        // Сумма
        summs: Number,
        date_create: {
            type: Date,
            default: Date.now
        },

        date_update: {
            type: Date,
            default: Date.now
        }
    }
);


const DocumentsModel = mongoose.model('Documents', Documents);
module.exports.CompanyModel = DocumentsModel;
module.exports.ClientsModel = ClientsModel;