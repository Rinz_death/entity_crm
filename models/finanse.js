'use strict';
const conf = require('../config/database.js');
const Schema = conf.Schema;
const mongoose = conf.mongoose;
/**
 * Модель с типами транзакци
 * @constructor
 * @param {String} [names="Зарплата"] тип транзакции
 * @param {String} [formula="summa*0.45/2"] формула расчета
 */
const TypeTransactions = new Schema(
    {
        names: String,
        // Формула для расчета налогов и всех издержок
        formula: String,
    }
)
const TypeTransactionsModel = mongoose.model('Transaction', TypeTransactions);

/**
 * Модель с типами транзакци
 * @constructor
 * @param {String} [number="a223ac"] номер транзакции
 * @param {Object} [types="refs"]  тип транзакции(ссылка)
 * @param {Object} [document="refs"] ссылка на документ(основание транзакции)
 * @param {Object} [salary="refs"] зарплата сотруднику(ссылка на сотрудника)
 */
// Различные транзакции
const Transaction = new Schema(
    {
        number: String,
        types: {
            type: mongoose.Schema.Types.ObjectId, 
            ref: 'TypeTransactions',
        },
        // На основании договора(трудового либо договора между юр <> юр(физ))
        document: [{
            type: mongoose.Schema.Types.ObjectId, 
            ref: 'Documents',
        }],
        // Прямой перевод сотруднику(ам), зарплата, премии и т.п.
        salary: [{
            type: mongoose.Schema.Types.ObjectId, 
            ref: 'Accounts',
        }],
    }
)


const TransactionModel = mongoose.model('Transaction', Transaction);
module.exports.TransactionModel = TransactionModel;
module.exports.TypeTransactionsModel = TypeTransactionsModel;