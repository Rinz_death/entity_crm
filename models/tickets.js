'use strict';
const conf = require('../config/database.js');
const Schema = conf.Schema;
const mongoose = conf.mongoose;
/**
 *
 * Модель с Задачами
 * @constructor
 * @param {Object} [documents="refs"] ссылка на соответствующий документ(договор)
 * @param {Object} [departament="refs"] Какой департамент в ответе за задачу(ссылка на отдел)
 * @param {Object} [accounts="refs"] Или конкретно человек ответственен за задачу(ссылка на account)
 * @param {Boolean} [done=False] выполнена или нет задача
 */
const Tickets = new Schema(
    {
        name: String,
        body: String,
        documents: {
            type: mongoose.Schema.Types.ObjectId, 
            ref: 'Documents',
        },
        
        // Обязан выполнить
        //сотрудник
        accounts: {
            type: mongoose.Schema.Types.ObjectId, 
            ref: 'Accounts',
        },
        // либо отдел
        departament: {
            type: mongoose.Schema.Types.ObjectId, 
            ref: 'Department',
        },
        ///

        // Выполнено ?
        done: Boolean,
        // Сроки
        terms: Date,


        date_create: {
            type: Date,
            default: Date.now
        },

        date_update: {
            type: Date,
            default: Date.now
        }
    }
)

const TicketsModel = mongoose.model('Tickets', Tickets);
module.exports.TicketsModel = TicketsModel;